/* jshint node: true */

/**
 * project
 * - lib
 * - node_modules
 * - src
 * - - css
 * - - images
 * - - js
 * - target
 * - - doc
 * - - dist
 * - - reports
 * - test
 * - - js
 * - vendor
 */

module.exports = function (grunt) {
    'use strict';

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        banner: '/*\n' +
        ' * <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
        ' * Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>; Licensed MIT\n' +
        ' */\n\n',
        meta: {
            version: '0.0.0'
        },

        // typescript: {
        //     base: {
        //         src: ['**/*.ts'],
        //         dest: './target/ts-js',
        //         rootDir: './src/ts',
        //         options: {
        //             module: 'amd', //or commonjs
        //             target: 'es3', //or es3
        //             sourceMap: false,
        //             declaration: false
        //         }
        //     }
        // },

        concat: {
            options: {
                // banner: '<%= banner %>',
                separator: ';'
            },
            dist: {
                src: ['src/js/**/*.js'],
                dest: 'target/dist/<%= pkg.name %>.js'
            }
        },

        uglify: {
            options: {
                banner: '<%= banner %>'
            },
            dist: {
                src: '<%= concat.dist.dest %>',
                dest: 'target/dist/<%= pkg.name %>.min.js'
            }
        },

        compress: {
            main: {
                options: {
                    mode: 'gzip',
                    pretty: true
                },
                src: '<%= uglify.dist.dest %>',
                dest: 'target/dist/'
            }
        },

        // connect : {
        //     options: {
        //         livereload: 35729,
        //         port: 9000
        //     },
        //     livetests : {
        //         options : {
        //             open: true,
        //             base : {
        //                 path : './',
        //                 options : {
        //                     index : 'tests.html'
        //                 }
        //             }
        //         }
        //     }
        // },

        clean: {
            folder: "target"
        },

        jscs: {
            src: ['Gruntfile.js', 'src/js/**/*.js', 'test/js/**/*.js'],
            options: {
                verbose: true, // If you need output with rule names http://jscs.info/overview.html#verbose
            }
        },

        jshint: {
            files: ['Gruntfile.js', 'src/js/**/*.js', 'test/js/**/*.js'],
            options: {
            }
        },

        jasmine: {
            test: {
                src: 'src/js/**/*.js',
                options: {
                    keepRunner : true,
                    specs: 'test/js/**/*Spec.js'
                }
            }
        },

        less: {
            production: {
                options: {
                    banner: '<%= banner %>',
                    // paths for @import directives
                    paths: [
                        'css/src'
                    ],
                    outputSourceFiles: true
                },
                files: {
                    'css/dist/global.css': 'css/src/global.less'
                }
            }
        },

        // watch: {
        //     files: ['src/js/**/*.js', 'test/js/**/*.js'],
        //     tasks: ['jshint', 'jasmine'],
        //     livereload : {
        //         options: {
        //             livereload : '<%= connect.options.livereload %>'
        //         },
        //         files : [
        //             'js/**/*.js',
        //             'tests/**/*.js',
        //             'tests.html'
        //         ]
        //     }
        // },

        jsdoc : {
            dist : {
                src: ['src/js/**/*.js'],
                options: {
                    destination: 'target/doc'
                }
            }
        }

    });


    // basics
    //grunt.loadNpmTasks('grunt-contrib-jshint');
    //grunt.loadNpmTasks('grunt-contrib-jasmine');
    // grunt.loadNpmTasks('grunt-karma');

    // css framework
    // grunt.loadNpmTasks('grunt-contrib-less');

    // default
    // grunt.loadNpmTasks('grunt-contrib-concat');
    // grunt.loadNpmTasks('grunt-contrib-watch');

    // tasks
    grunt.registerTask('test', ['jscs', 'jshint', 'jasmine']);
    grunt.registerTask('live', ['test', 'connect:livetests', 'watch']);
    grunt.registerTask('dist', ['test', 'concat', 'uglify', 'compress']);
    grunt.registerTask('default', ['dist']);

};