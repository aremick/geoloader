// /* globals adobe: true*/
// var adobe = adobe || {};

// adobe.Locale = (function () {
//     'use strict';

//     //CQ specific array of Locales structured
//     //{country/region code}_{language} approximately
//     // Yes this looks a little odd, but lookups against {}
//     // are WAY faster than lookups against [].
//     var _GEO_CODES = {'africa': null, 'at': null, 'au': null,
//         'be_en': null, 'be_fr': null, 'be_nl': null, 'bg': null,
//         'br': null, 'ca': null, 'ca_fr': null, 'ch_de': null,
//         'ch_fr': null, 'ch_it': null, 'cis': null, 'cn': null,
//         'cy_en': null, 'cz': null, 'de': null, 'dk': null, 'ee': null,
//         'eeurope': null, 'es': null, 'fi': null, 'fr': null,
//         'gr_en': null, 'hk_en': null, 'hk_zh': null, 'hr': null,
//         'hu': null, 'ie': null, 'il_en': null, 'il_he': null,
//         'in': null, 'it': null, 'jp': null, 'kr': null, 'la': null,
//         'lt': null, 'lu_de': null, 'lu_en': null, 'lu_fr': null,
//         'lv': null, 'mena_ar': null, 'mena_en': null, 'mena_fr': null,
//         'mt': null, 'mx': null, 'nl': null, 'no': null, 'nz': null,
//         'pl': null, 'pt': null, 'ro': null, 'rs': null, 'ru': null,
//         'se': null, 'sea': null, 'si': null, 'sk': null, 'tr': null,
//         'tw': null, 'ua': null, 'uk': null},

//     //Map of non-English search engines commonly used in Regions
//     //outside the US to CQ specific Locale codes
//     _SEARCH_ENGINES: {'google.fr': 'fr', 'google.co.uk': 'uk',
//         'google.com.au': 'au', 'google.co.jp': 'jp', 'google.de': 'de',
//         'google.ca': ['ca', 'ca_fr'], 'google.co.id': 'sea',
//         'google.co.in': 'in', 'google.com.ph': 'sea',
//         'google.com.tr':'tr', 'google.nl': 'nl',
//         'au.search.yahoo.com': 'au', 'fr.search.yahoo.com': 'fr',
//         'uk.search.yahoo.com': 'uk', 'search.yahoo.co.jp': 'jp',
//         'yandex.ru': 'ru', 'de.search.yahoo.com': 'de'},

//     //
//     _GEO_CODE_COOKIE_NAME = 'international',
//     _DEFAULT_GEO_CODE = 'us',
//     pathGeoCode,
//     cookieGeoCode
//     ;

//     function isValidGeoCode(code) {
//         return _GEO_CODES[code] == null;
//     }

//     function getCountryBySearchEngine(searchEngine) {
//         return _SEARCH_ENGINES[searchEngine];
//     }

//     return {
//         isValidGeoCode: isValidGeoCode,
//         getCountryBySearchEngine, getCountryBySearchEngine,
//         getGeoCode: getGeoCode
//     }

// })();