/* globals adobe: true*/
var adobe = adobe || {};

adobe.Env = (function () {
    'use strict';

    var _ENV_HOSTS = {
            'LOCAL': ['127.0.0.1', 'localhost'],
            'DEV': ['dev'],
            'QA': ['qa'],
            'STAGE': ['stage']
        },
        _envCache = {};

    function isProd(host) {
        return getEnv(host) === 'PROD';
    }

    /**
     * Trys to identify what env we are running in.
     *
     * @param  {string} [host=window.location.hostname] the hostname
     * to match against.
     * @return {string} An env label. one of: LOCAL, DEV, QA, STAGE, or PROD.
     */
    function getEnv(host) {
        var env, hostString;
        // host defaults to location.hostname if not set.
        // Either way, set it to lowercase
        host = (host ? host : window.location.hostname).toLowerCase();

        // kind of a lot of boogie to do all this matching so once we
        // do a match, cache it in case we are asked again.

        // check the cache
        if (_envCache[host] !== undefined ) {
            return _envCache[host];
        }

        // not found in cache. Check it against all the _ENV_HOST
        // hostnames for each env.
        for (env in _ENV_HOSTS) {
            if (_ENV_HOSTS.hasOwnProperty(env)) {
                for (var i = _ENV_HOSTS[env].length - 1; i >= 0; i--) {
                    hostString = _ENV_HOSTS[env][i];
                    if (host.indexOf(_ENV_HOSTS[env][i]) > -1) {
                        // we have a match! Cache it.
                        _envCache[host] = env;
                        // and we're done looking.
                        return env;
                    }
                }
            }
        }

        // if didn't match a non_prod host, it must be prod
        // cache it
        _envCache[host] = _envCache[host] || 'PROD';

        // and return it
        return _envCache[host];
    }

    function getCookieHost(host) {
        return isProd(host) ? host : '';
    }

    // Reveal public pointers to
    // private functions and properties
    return {
        getEnv: getEnv,
        isProd: isProd
    };

})();
