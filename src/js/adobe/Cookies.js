/* globals adobe: true*/
var adobe = adobe || {};

adobe.cookies = (function () {
    'use strict';

    var _DEFAULT_COOKIE_DOMAIN = '.adobe.com';

    /**
     *
     *
     * @param  {string} host - host to validate
     * @return {string}      [description]
     */
    function getDomain(host) {
        if (adobe.Env.isProd()) {
            return _DEFAULT_COOKIE_DOMAIN;
        }
        return document.location.hostname;
    }


    /**
     * checks the international cookie for a valid geo_code. If not found,
     * or not valid, returns default.
     *
     * @return {string} geo code.
     */
    function getGeoCode() {
        var geoCode = remick.cookies.getCookie(_GEO_CODE_COOKIE_NAME);
        if (adobe.geo.isValidGeoCode(geoCode)) {
            return geoCode;
        }
        return undefined;
    }


    return {
        getDomain: getDomain
    };

})();
