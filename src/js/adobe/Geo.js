// /* globals adobe: true*/
// /* globals remick: true*/

// var adobe;
// (function (adobe) {
//     var geo;
//     (function (geo) {
//         'use strict';

//         _DEFAULT_GEO_CODE = 'us';

//         _GEO_CODES = {'africa': 1, 'at': 1, 'au': 1, 'be_en': 1, 'be_fr': 1,
//             'be_nl': 1, 'bg': 1, 'br': 1, 'ca': 1, 'ca_fr': 1, 'ch_de': 1,
//             'ch_fr': 1, 'ch_it': 1, 'cis': 1, 'cn': 1, 'cy_en': 1, 'cz': 1,
//             'de': 1, 'dk': 1, 'ee': 1, 'eeurope': 1, 'es': 1, 'fi': 1, 'fr': 1,
//             'gr_en': 1, 'hk_en': 1, 'hk_zh': 1, 'hr': 1, 'hu': 1, 'ie': 1,
//             'il_en': 1, 'il_he': 1, 'in': 1, 'it': 1, 'jp': 1, 'kr': 1, 'la': 1,
//             'lt': 1, 'lu_de': 1, 'lu_en': 1, 'lu_fr': 1, 'lv': 1, 'mt': 1,
//             'mena_ar': 1, 'mena_en': 1, 'mena_fr': 1, 'mx': 1, 'nl': 1, 'no': 1,
//             'nz': 1, 'pl': 1, 'pt': 1, 'ro': 1, 'rs': 1, 'ru': 1, 'se': 1,
//             'sea': 1, 'si': 1, 'sk': 1, 'tr': 1, 'tw': 1, 'ua': 1, 'uk': 1};

//         _NON_PROD_HOSTS = {'127.0.0.1': 1, 'localhost': 1, 'dev': 1, 'qa': 1,
//             'stage': 1};

//         function validateGeoCode(code) {
//             code = code.toLowerCase();
//             return _GEO_CODES[code] ? code : undefined;
//         }

//         function checkForRedirect() {
//             var hashGeoCode, cookieGeoCode, pathGeoCode, redirectPath, cString,
//             maxAge, now, cleanPath;

//             // check the hash for geo info
//             hashGeoCode = window.location.hash.match(/[#&]international=([^\s&]*)/)[1];
//             hashGeoCode = validateGeoCode(hashGeoCode);

//             // If there is a hash geo code, set the international cookie
//             if (hashGeoCode) {
//                 // calc expiration
//                 maxAge = 60 * 60 * 24;
//                 now = new Date();
//                 now.setTime(maxAge * 1000 + now.getTime());

//                 // check for non-prod env
//                 isProd = document.location.hostname.match(/(qa|dev|stage|localhost)/) === null;

//                 cString = 'international=' + hashGeoCode + '';
//                 if (isProd) {
//                     cString.append(';domain=.adobe.com');
//                 }
//                 cString.append(';max-age=', maxAge);
//                 cString.append(';expires=', now.toUTCString());

//                 document.cookie = cString;
//             }

//             //get the value cookie named international
//             cookieGeoCode = document.cookie.match(/[\s;]*international=([^\s;]*)/)[1];
//             cookieGeoCode = validateGeoCode(cookieGeoCode);

//             // check the path for geo code.
//             pathGeoCode = _GEO_CODES[window.location.path.split('/')[1]];
//             pathGeoCode = validateGeoCode(pathGeoCode);

//             // reload the page if the cookie and path geoCodes
//             // don't match up.
//             if (cookieGeoCode !== _DEFAULT_GEO_CODE &&
//                 cookieGeoCode !== pathGeoCode) {
//                 // clean the url
//                 if (pathGeoCode) {
//                     cleanPath = window.location.path.split('/').splice(1,1).join('/');
//                 }
//                 // see if the page exists


//                 fullRequest.open('HEAD', fullUrl, true);
//                 fullRequest.onreadystatechange = function () {
//                     if (fullRequest.readyState !== 4) {
//                         return;
//                     }
//                     if (Math.floor(indexRequest.status / 100) === 2) {
//                         // full url is valid
//                         fullUrlValid = true;
//                         // window.location = fullUrl;
//                         console.log('go to:' + fullUrl);
//                     } else {
//                         // full url is invalid
//                         fullUrlValid = false;
//                         if (indexUrlValid) {
//                             // window.location = indexUrl;
//                             console.log('go to:' + indexUrl);
//                         }
//                     }
//                 };
//                 fullRequest.setRequestHeader('Content-type', 'text/plain; charset=utf-8');
//                 fullRequest.send();


//                 indexRequest.open('HEAD', indexUrl, true);
//                 indexRequest.onreadystatechange = function () {
//                     if (indexRequest.readyState !== 4) {
//                         return;
//                     }
//                     if (Math.floor(indexRequest.status / 100) === 2) {
//                         // index url is valid
//                         indexUrlValid = true;
//                         if (fullUrlValid === false) {
//                             // window.location = indexUrl;
//                             console.log('go to:' + indexUrl);
//                         }
//                     } else {
//                         // index url is invalid
//                         indexUrlValid = false;
//                         if (fullUrlValid == false) {
//                             // ack!! both urls are false!! can't redirect!
//                             console.log('dont know where to go');
//                         }
//                     }
//                 };
//                 indexRequest.setRequestHeader('Content-type', 'text/plain; charset=utf-8');
//                 indexRequest.send();


//             }

//         }


//         geo.checkForRedirect: checkForRedirect;

//     })(geo || (geo = {}));
// })(adobe || (adobe = {}));
