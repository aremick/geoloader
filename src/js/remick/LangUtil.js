// set up the namespace
var remick = remick || {};

/**
 * Some language enhancing utilities.
 * @module remick/LangUtil
 */
remick.LangUtil = (function () {
    'use strict';

    // Reveal public pointers to
    // private functions and properties
    return {

        isUndefined: function(thing) {
            return typeof thing === 'undefined';
        },

        isNotNumber: function(thing) {
            return typeof thing === 'number' && isNaN(thing);
        },

        isNull: function(thing) {
            return typeof thing === 'object' && thing === null;
        },

        /**
         * More specific than falsy. '', 0, and -0 will evaluate
         * as false so that functions that want to validly return
         * these can do so, and be tested for problematic response.
         *
         * @param thing - thing to be tested
         * @return {Boolean} will return true if thing is null,
         * undefined, NaN. Otherwise it will return false.
         */
        isNullish: function (thing) {
            var type = typeof thing;


            return (type === 'undefined') ||
                (type === 'object' && thing === null) ||
                (type === 'number' && isNaN(thing));
        },

        /**
         * A safer version of toString(). To avoid getting 'NaN' or other
         * non-real string values. It will coerse strings out of booleans,
         * numbers, objects with their own toString() function, and of course
         * strings.
         *
         * @param thing - thing to be converted to string.
         * @return {string} the coersed string, null if unable.
         */
        getStringSafely: function (thing) {
            var type;
            // if it is nullish, return null.
            if (this.isNullish(thing)) {
                return null;
            }

            type = typeof thing;

            // it is already a string, return it.
            if (type === 'string') {
                return thing;
            }

            // convert numbers and booleans to strings.
            if (type === 'number' || type === 'boolean') {
                return String(thing);
            }

            // it is an object with it's own toString implementation
            // hopfully it is meaningful.
            if (type === 'object' &&
                thing.hasOwnProperty('toString') &&
                typeof thing.toString === 'function') {
                return thing.toString();
            }

            // no telling what it is, return null.
            return null;
        },

        /**
         * Using the same rules as getStringSafely returns true if
         * a string can be safely coersed, false if not.
         *
         * @param  {} thing - thing to be tested.
         * @return {Boolean} - true if it can be safely coersed into a
         * string. false if not.
         */
        isStringy: function (thing) {
            return this.getStringSafely(thing) !== null;
        }
    };
})();
