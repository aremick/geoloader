// set up the namespace
var remick = remick || {};

/**
* create a StringMap. If both format and srcString are
* submitted as param then the StringMap is initialized
* with the parsed srcString.
*
* @constructor
* @param {string} [format] - type of string to parse ('query' or 'cookie').
* @param {string} [srcString] - string to parse. as returned by document.cookie
* or location.search
**/
remick.StringMap = function (format, srcString) {
    'use strict';
    var stringMap = {},
        FORMATS = { // legal formats for parsing and assembling strings
            query: {primaryDelimiter: '&', secondaryDelimiter: '='},
            cookie: {primaryDelimiter: ';', secondaryDelimiter: '='}
        };


    /**
     * Adds items to the map. Always returns the StringMap itself
     * for chaining. If only key is defined, then key is added with
     * an empty array of values. If key and value are defined, then
     * the value is added to the key's array of values and sorted.
     *
     * @param  {string} key - the targeted key.
     * @param  {string} [value] - the targeted value.
     * @return {object} returns self for chaining.
     */
    this.put = function (key, value) {
        if (typeof key === 'string') {
            if (!stringMap[key]) {
                stringMap[key] = [];
            }
            if (typeof value === 'string') {
                stringMap[key].push(value);
                stringMap[key].sort();
                return this;
            }
            return this;
        }
        return null;
    };

    /**
     * Always returns an array of values. If no key parameter is
     * given, then a sorted list of keys is returned. If the key
     * parm is defined, the values of that key are returned.
     *
     * @param  {string} [key] - the targeted key.
     * @return {array} The values for key if specified, the set of all keys
     * if not.
     */
    this.get = function (key) {
        var keys = [], akey;

        if (typeof key === 'string') {
            // key param is defined so just return it's values.
            // if the key is not found it will return undefined.
            return stringMap[key];
        }

        // key was not specified, so make a list of all keys
        // and return it.
        for (akey in stringMap) {
            if (stringMap.hasOwnProperty(akey)) {
                keys.push(akey);
            }
        }
        return keys.sort();
    };

    /**
     * Removes items from the map. Always returns the StringMap
     * itself for chaining.
     *
     * @param  {string} key - The targeted key.
     * @param  {string} [value] - The targeted value.
     * @return {object} returns self for chaining.
     */
    this.delete = function (key, value) {
        var index;
        if (typeof key === 'string') {
            if (typeof value === 'string') {
                index = stringMap[key].indexOf(value);
                if (index > -1) {
                    stringMap[key].splice(index, index + 1);
                    return this;
                }
                return this;
            }
            delete stringMap[key];
            return this;
        }
        return this;
    };

    /**
     * get the number of keys or values associated with a key, in the
     * StringMap.
     *
     * @param  {string} [key] - specifies which element of the object
     * to check.
     * @return {number} - If the key is not specified, the number of
     * keys is returned. If key is specified, and exists in the
     * StringMap, the number of values under that key is returned.
     * If the key is specified but not found, undefined is returned.
     */
    this.getLength = function (key) {
        var toBeMeasured = this.get(key);
        if (!toBeMeasured) {
            return undefined;
        }
        return toBeMeasured.length;
    };

/**
     * Verify the existance of a given key or key/value pair within
     * the StringMap.
     *
     * @param  {string} key - specifies which element of the object
     * to check.
     * @param  {string} [value] - specifies which element belonging to
     * key to check for.
     * @return {boolean} - if value is specified, returns the existance of
     * that value under given key. If value is not specified, returns the
     * existance of the given key.
     */
    this.exists = function (key, value) {
        if (stringMap[key]) {
            if (value) {
                return this.get(key).indexOf(value) > -1;
            }
            return true;
        }
        return false;
    };

    /**
     * Returns an object containing the delimiters used by the specified
     * format.
     *
     * @param  {string} format - Label for the format to parse the
     *  src string with. (currently valid: 'cookie', 'query')
     * @return {object} - An object with two properties: 'primaryDelimiter',
     * and 'secondaryDelimiter'. If format is unrecognized, returns undefined.
     */
    this.getFormat = function (format) {
        return FORMATS[format];
    };

    /**
     * Will separate the srcString into keys and values and add them
     * to the map. Any existing keys and values will be left in tact.
     *
     * @param {string} format - Label for the format to parse the
     *  src string with. (currently valid: 'cookie', 'query')
     * @param {string} srcString - String to be parsed
     * @return {Object} - returns this object for chaining. returns null
     * if format is unrecognized.
     */
    this.parseString = function (format, srcString) {
        var delim, pairs, i, keyVal;

        // validate the format
        if (!format || !this.getFormat(format)) {
            return null;
        }

        // parse the string
        delim = this.getFormat(format);
        if (srcString && delim) {
            pairs = srcString.split(delim.primaryDelimiter);
            for (i = pairs.length - 1; i >= 0; i--) {
                keyVal = pairs[i].split(delim.secondaryDelimiter, 2);
                this.put(keyVal[0], keyVal[1]);
            }
        }
        return this;
    };

    /**
     * Using the keys and values currently in the StringMap, joins
     * them together using the delimiters appropriate to the format
     * specified.
     *
     * @param  {string} format - Label for the format to parse the
     *  src string with. (currently valid: 'cookie', 'query')
     * @return {string} - current string map state joined together
     * into a single string using specified format.
     */
    this.unparseString = function (format) {
        var values, valuesLength, i, j,
            pairs = [],
            keys = this.get(), keysLength = keys.length,
            delim = this.getFormat(format);

        // make sure we found delimiters for the given format
        if (!delim) {
            return null;
        }

        //
        for (i = 0; i < keysLength; i++) {
            values = this.get(keys[i]);
            valuesLength = values.length;
            if (valuesLength === 0) {
                pairs.push(keys[i]);
            } else {
                for (j = 0; j < valuesLength; j++) {
                    pairs.push([keys[i], values[j]].join(delim.secondaryDelimiter));
                }
            }
        }
        return pairs.join(delim.primaryDelimiter);
    };

    this.parseString(format, srcString);

};
