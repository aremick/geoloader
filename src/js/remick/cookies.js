// set up the namespace
var remick = remick || {};
remick.cookies = remick.cookies || {};

/**
 * Some utilities for working with cookies
 *
 * @module remick/cookies
 */
remick.cookies = (function() {
    return {

        /**
         * Parse and decode values returned by the browser Document.cookie
         * into an object of name value pairs.
         *
         * @static
         * @param {string} name name of the cookie you are looking for
         * @param  {string} [cString] - cookie string, as returned by document.cookie.
         * If undefined, it will be pulled from document.cookie.
         * @return {object} An object with all the cookie values as parsed into
         * name value pairs and decoded.
         */
        getCookie: function (name, cString) {
            var i,
                nameValue,
                nvPair,
                nvPairs,
                cookieObj = {};

            cString = cString !== undefined ? cString : document.cookie;
            nvPairs = cString.replace(/\s+/g, '').split(';');

            for (i = nvPairs.length - 1; i >= 0; i--) {
                nvPair = nvPairs[i];
                // if pair is empty, skip it.
                if (nvPair.length > 0) {
                    // split pair into 1 or 2 values
                    nameValue = nvPair.split('=', 2);
                    // console.log('0: ' + nameValue[0] + ' 1: ' + nameValue[1] + ' (' + nameValue.length + ')');
                    if (nameValue.length === 1) {
                        // there was only 1 value,
                        cookieObj[''] = decodeURIComponent(nameValue[0]);
                    } else {
                        // there were 2 values. decode both and add to the cookieObj
                        cookieObj[decodeURIComponent(nameValue[0])] = decodeURIComponent(nameValue[1]);
                    }
                }
            }
            // console.log(cookieObj['']);
            return typeof name === 'string' ? cookieObj[name] : cookieObj;
        },

        /**
         * Formats the values from spec into a well formed,
         * encoded Cookie String.
         *
         * @param  {object} spec
         * @param {string} [spec.name = ''] - name of the cookie
         * @param {string} [spec.value = ''] - value of the cookie
         * @param {string} [spec.path = ''] - path of the cookie. If empty,
         * path will not be specified.
         * @param {string} [spec.domain = ''] - domain of the cookie. If
         * empty, domain will not be specified.
         * @param {number} [spec.maxAge = undefined] - maxAge of the
         * cookie. If defined, both max-age and expires will be specified
         * (equivilantly). If undefined, niether will be specified.
         * @param {boolean} [spec.secure = false] - if true secure flag will
         * be set.
         * @param  {boolean} [commit = false] - if truthy, will submit the cookie to
         * document.cookie.
         * @return {string} the string, ready for submission to document.cookie.
         */
        makeCookie: function(spec, commit) {
            var date, cString, cookieArray = [];

            // make sure there is a minimal spec
            spec = spec ? spec : {};
            spec.name = spec.name ? spec.name : '';
            spec.value = spec.value ? spec.value : '';

            // the name and value params need to be a strings or default to ''.
            cookieArray.push(encodeURIComponent(spec.name) + '=' + encodeURIComponent(spec.value));

            if (spec.path) {
                cookieArray.push('path=' + spec.path);
            }

            if (spec.domain) {
                cookieArray.push('domain=' + spec.domain);
            }

            if (spec.maxAge !== undefined) {
                // calculate the date for expires
                date = new Date();
                date.setTime(spec.maxAge * 1000 + date.getTime());
                // set expires for IE.
                cookieArray.push('expires=' + date.toUTCString());
                // set max-age for everyone else.
                cookieArray.push('max-age=' + spec.maxAge);
            }

            if (spec.secure) {
                cookieArray.push('secure');
            }

            cString = cookieArray.join(';');

            if (commit) {
                document.cookie = cString;
            }

            return cString;
        },

        /**
         * Creates a string formated and with the correct values for deleteing
         * a cookie
         * @static
         * @param  {string} name - the name of the cookie to delete.
         * @param {boolean} [commit] - if true, string will be submitted
         * to document.cookie.
         * @return {string} A string, that when passed to document.cookie will
         * expire the cookie.
         */
        deleteCookie: function(name, commit) {
            return remick.cookies.makeCookie({'name': name, 'maxAge': -99}, commit);
        }
    };
})();
