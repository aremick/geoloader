module adobe {
	module geo {

		enum GeoCode {'africa'=1, 'at', 'au', 'be_en', 'be_fr', 'be_nl',
			'bg', 'br', 'ca', 'ca_fr', 'ch_de', 'ch_fr', 'ch_it', 'cis', 'cn',
			'cy_en', 'cz', 'de', 'dk', 'ee', 'eeurope', 'es', 'fi', 'fr',
			'gr_en', 'hk_en', 'hk_zh', 'hr', 'hu', 'ie', 'il_en', 'il_he',
			'in', 'it', 'jp', 'kr', 'la', 'lt', 'lu_de', 'lu_en', 'lu_fr',
			'lv', 'mt', 'mena_ar', 'mena_en', 'mena_fr', 'mx', 'nl', 'no',
			'nz', 'pl', 'pt', 'ro', 'rs', 'ru', 'se', 'sea', 'si', 'sk', 'tr',
			'tw', 'ua', 'uk'};

		export function getGeoCookie(cookies:string = document.cookie):string {
			return cookies.match(/[\s;]*inter=([^\s,;]*)/)[1];
		}

	}
}
