/* jshint jasmine: true */

describe('StringMap Tests:', function() {
    'use strict';

    describe('StringMap creator tests: ', function () {

        it('Create a StringMap without params', function () {
            var sm = new remick.StringMap();
            expect(sm).toBeDefined();
            expect(sm.get()).toEqual([]);
            expect(sm.getLength()).toEqual(0);
        });
    });

    describe('StringMap.put should: ', function () {

        var sm;

        beforeEach(function() {
            sm = new remick.StringMap();
        });

        afterEach(function() {
            sm = null;
        });

        it('put a key', function () {
            expect(sm.put('a')).toBe(sm);

            expect(sm.get('a')).toEqual([]);
            expect(sm.get()).toEqual(['a']);
            expect(sm.exists('a')).toBe(true);
            expect(sm.getLength()).toEqual(1);
        });

        it('put a key and a value', function () {
            expect(sm.put('a', 'foo')).toBe(sm);

            expect(sm.get()).toEqual(['a']);
            expect(sm.get('a')).toEqual(['foo']);
            expect(sm.exists('a', 'foo')).toBe(true);
            expect(sm.getLength()).toEqual(1);
            expect(sm.getLength('a')).toEqual(1);
        });

        it('put a key and two values', function () {
            expect(sm.put('a', 'foo').put('a', 'bar')).toBe(sm);

            expect(sm.get()).toEqual(['a']);
            expect(sm.get('a')).toEqual(['bar','foo']);
            expect(sm.exists('a', 'foo')).toBe(true);
            expect(sm.exists('a', 'bar')).toBe(true);
            expect(sm.getLength()).toEqual(1);
            expect(sm.getLength('a')).toEqual(2);
        });

        it('handle empty strings as keys.', function () {
            expect(sm.put('')).toBe(sm);
            expect(sm.get('')).toEqual([]);
            expect(sm.put('', 'foo')).toBe(sm);
            expect(sm.get('')).toEqual(['foo']);
        });

        it('handle empty strings as values.', function () {
            expect(sm.put('c', '')).toBe(sm);
            expect(sm.get('c')).toEqual(['']);
        });

        it('not accept booleans.', function () {
            sm.put('c', true).put('c', false);
            expect(sm.get('c')).toEqual([]);
        });

        it('not accept null.', function () {
            sm.put('c', null);
            expect(sm.get('c')).toEqual([]);
        });

        it('not accept NaN.', function () {
            sm.put('c', NaN);
            expect(sm.get('c')).toEqual([]);
        });

        it('not accept undefined.', function () {
            sm.put('c', undefined);
            expect(sm.get('c')).toEqual([]);
        });

        it('not accept numbers.', function () {
            sm.put('c', 4);
            expect(sm.get('c')).toEqual([]);
        });
    });

    describe('StringMap.get should: ', function () {

        var sm;

        beforeEach(function() {
            sm = new remick.StringMap();
            sm.put('a').put('b', 'foo').put('c', 'baz').put('c', 'bar');
            sm.put('', 'bar').put('d', '');
        });

        afterEach(function() {
            sm = null;
        });

        it('return undef when the key has not been defined', function () {
            expect(sm.get('z')).toBeUndefined();
        });

        it('return an empty array when the key has not been assigned a value.', function () {
            expect(sm.get('a')).toEqual([]);
        });

        it('return an array of one value when one value has been assigned to the key', function () {
            expect(sm.get('b')).toEqual(['foo']);
        });

        it('return a sorted array when multiple values have been assigned to a key.', function () {
            expect(sm.get('c')).toEqual(['bar', 'baz']);
        });

        it('handle empty string as key.', function () {
            expect(sm.get('')).toEqual(['bar']);
        });

        it('handle empty string as value.', function () {
            expect(sm.get('d')).toEqual(['']);
        });
    });

    describe('StringMap.delete should: ', function () {

        var sm;

        beforeEach(function() {
            sm = new remick.StringMap();
            sm.put('a').put('b', 'foo').put('c', 'baz');
            sm.put('c', 'bar').put('', 'bar').put('d', '');
        });

        afterEach(function() {
            sm = null;
        });

        it('leave the map alone when the key has not been defined.', function () {
            expect(sm.delete('z')).toBe(sm);
            expect(sm.get()).toEqual(['', 'a', 'b', 'c', 'd']);
        });

        it('remove the key when the key has not been assigned a value.', function () {
            sm.delete('a');
            expect(sm.get()).toEqual(['', 'b', 'c', 'd']);
        });

        it('remove the key when one value has been assigned.', function () {
            sm.delete('b');
            expect(sm.get()).toEqual(['', 'a', 'c', 'd']);
        });

        it('remove the key when multiple values have been assigned.', function () {
            sm.delete('c');
            expect(sm.get()).toEqual(['', 'a', 'b', 'd']);
        });

        it('handle empty string as key.', function () {
            sm.delete('');
            expect(sm.get()).toEqual(['a', 'b', 'c', 'd']);
        });

        it('remove the value from a list of multiple values.', function () {
            sm.delete('c', 'baz');
            expect(sm.get('c')).toEqual(['bar']);
        });

        it('handle empty string as value.', function () {
            sm.delete('d', '');
            expect(sm.get()).toEqual(['', 'a', 'b', 'c', 'd']);
        });
    });

    describe('StringMap.getFormat should: ', function () {

        it('return the correct delimiters.', function () {
            var sm = new remick.StringMap();
            expect(sm.getFormat('query')).toEqual({primaryDelimiter: '&', secondaryDelimiter: '='});
            expect(sm.getFormat('cookie')).toEqual({primaryDelimiter: ';', secondaryDelimiter: '='});
        });

        it('return undefined for unrecognized delimiters.', function () {
            var sm = new remick.StringMap();
            expect(sm.getFormat('')).toEqual(undefined);
            expect(sm.getFormat('foobar')).toEqual(undefined);
        });
    });

    describe('StringMap.parseString should: ', function () {

        it('parsing with the query format', function () {
            var sm = new remick.StringMap();
            sm.parseString('query', 'a&b=&c=foo&d=bar&d=baz&=foo');
            expect(sm.get()).toEqual(['', 'a', 'b', 'c', 'd']);
            expect(sm.get('')).toEqual(['foo']);
            expect(sm.get('a')).toEqual([]);
            expect(sm.get('b')).toEqual(['']);
            expect(sm.get('c')).toEqual(['foo']);
            expect(sm.get('d')).toEqual(['bar', 'baz']);
        });

        it('with the cookie format', function () {
            var sm = new remick.StringMap();
            sm.parseString('cookie', 'a;b=;c=foo;d=bar;d=baz;=foo');
            expect(sm.get()).toEqual(['', 'a', 'b', 'c', 'd']);
            expect(sm.get('')).toEqual(['foo']);
            expect(sm.get('a')).toEqual([]);
            expect(sm.get('b')).toEqual(['']);
            expect(sm.get('c')).toEqual(['foo']);
            expect(sm.get('d')).toEqual(['bar', 'baz']);

        });
    });

    describe('StringMap.unparseString should: ', function () {

        it('with the query format', function () {
            var sm = new remick.StringMap('query', 'a&b=&c=foo&d=bar&d=baz&=foo');
            expect(sm.unparseString('query')).toEqual('=foo&a&b=&c=foo&d=bar&d=baz');
        });

        it('with the cookie format', function () {
            var sm = new remick.StringMap('query', 'a&b=&c=foo&d=bar&d=baz&=foo');
            expect(sm.unparseString('cookie')).toEqual('=foo;a;b=;c=foo;d=bar;d=baz');
        });
    });
});
