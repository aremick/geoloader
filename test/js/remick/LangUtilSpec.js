/* jshint jasmine: true */

describe('LangUtil Tests:', function() {
    'use strict';

    describe('isNotNumber should: ', function () {

        it('return false if undefined', function () {
            expect(remick.LangUtil.isNotNumber(undefined)).toBe(false);
            var obj = {};
            expect(remick.LangUtil.isNotNumber(obj.foo)).toBe(false);
        });

        it('return false if null', function () {
            expect(remick.LangUtil.isNotNumber(null)).toBe(false);
        });

        it('return true if Nan', function () {
            expect(remick.LangUtil.isNotNumber(Number.NaN)).toBe(true);
            expect(remick.LangUtil.isNotNumber(1/'a')).toBe(true);
        });

        it('return false for any type of boolean', function () {
            expect(remick.LangUtil.isNotNumber(true)).toBe(false);
            expect(remick.LangUtil.isNotNumber(false)).toBe(false);
        });

        it('return false for an empty string', function () {
            expect(remick.LangUtil.isNotNumber('')).toBe(false);
        });

        it('return false for any string', function () {
            expect(remick.LangUtil.isNotNumber('a')).toBe(false);
        });

        it('return false for 0', function () {
            expect(remick.LangUtil.isNotNumber(0)).toBe(false);
        });

        it('return false for -0', function () {
            expect(remick.LangUtil.isNotNumber(-0)).toBe(false);
        });

        it('return false for any number', function () {
            expect(remick.LangUtil.isNotNumber(876)).toBe(false);
        });

    });

    describe('isUndefined should: ', function () {

        it('return true if undefined', function () {
            expect(remick.LangUtil.isUndefined(undefined)).toBe(true);
            var obj = {};
            expect(remick.LangUtil.isUndefined(obj.foo)).toBe(true);
        });

        it('return false if null', function () {
            expect(remick.LangUtil.isUndefined(null)).toBe(false);
        });

        it('return false if Nan', function () {
            expect(remick.LangUtil.isUndefined(Number.NaN)).toBe(false);
            expect(remick.LangUtil.isUndefined(1/'a')).toBe(false);
        });

        it('return false for any type of boolean', function () {
            expect(remick.LangUtil.isUndefined(true)).toBe(false);
            expect(remick.LangUtil.isUndefined(false)).toBe(false);
        });

        it('return false for an empty string', function () {
            expect(remick.LangUtil.isUndefined('')).toBe(false);
        });

        it('return false for any string', function () {
            expect(remick.LangUtil.isUndefined('a')).toBe(false);
        });

        it('return false for 0', function () {
            expect(remick.LangUtil.isUndefined(0)).toBe(false);
        });

        it('return false for -0', function () {
            expect(remick.LangUtil.isUndefined(-0)).toBe(false);
        });

        it('return false for any number', function () {
            expect(remick.LangUtil.isUndefined(876)).toBe(false);
        });

    });

    describe('isNull should: ', function () {

        it('return false if undefined', function () {
            expect(remick.LangUtil.isNull(undefined)).toBe(false);
            var obj = {};
            expect(remick.LangUtil.isNull(obj.foo)).toBe(false);
        });

        it('return true if null', function () {
            expect(remick.LangUtil.isNull(null)).toBe(true);
        });

        it('return false if Nan', function () {
            expect(remick.LangUtil.isNull(Number.NaN)).toBe(false);
            expect(remick.LangUtil.isNull(1/'a')).toBe(false);
        });

        it('return false for any type of boolean', function () {
            expect(remick.LangUtil.isNull(true)).toBe(false);
            expect(remick.LangUtil.isNull(false)).toBe(false);
        });

        it('return false for an empty string', function () {
            expect(remick.LangUtil.isNull('')).toBe(false);
        });

        it('return false for any string', function () {
            expect(remick.LangUtil.isNull('a')).toBe(false);
        });

        it('return false for 0', function () {
            expect(remick.LangUtil.isNull(0)).toBe(false);
        });

        it('return false for -0', function () {
            expect(remick.LangUtil.isNull(-0)).toBe(false);
        });

        it('return false for any number', function () {
            expect(remick.LangUtil.isNull(876)).toBe(false);
        });

    });

    describe('isNullish should: ', function () {

        it('return true if undefined', function () {
            expect(remick.LangUtil.isNullish(undefined)).toBe(true);
            var obj = {};
            expect(remick.LangUtil.isNullish(obj.foo)).toBe(true);
        });

        it('return true if null', function () {
            expect(remick.LangUtil.isNullish(null)).toBe(true);
        });

        it('return true if Nan', function () {
            expect(remick.LangUtil.isNullish(Number.NaN)).toBe(true);
            expect(remick.LangUtil.isNullish(1/'a')).toBe(true);
        });

        it('return false for any type of boolean', function () {
            expect(remick.LangUtil.isNullish(true)).toBe(false);
            expect(remick.LangUtil.isNullish(false)).toBe(false);
        });

        it('return false for an empty string', function () {
            expect(remick.LangUtil.isNullish('')).toBe(false);
        });

        it('return false for any string', function () {
            expect(remick.LangUtil.isNullish('a')).toBe(false);
        });

        it('return false for 0', function () {
            expect(remick.LangUtil.isNullish(0)).toBe(false);
        });

        it('return false for -0', function () {
            expect(remick.LangUtil.isNullish(-0)).toBe(false);
        });

        it('return false for any number', function () {
            expect(remick.LangUtil.isNullish(876)).toBe(false);
        });

    });

    describe('getStringSafely should: ', function () {

        it('return null if undefined', function () {
            expect(remick.LangUtil.getStringSafely(undefined)).toBe(null);
            expect(remick.LangUtil.getStringSafely({}.foo)).toBe(null);
        });

        it('return null if null', function () {
            expect(remick.LangUtil.getStringSafely(null)).toBe(null);
        });

        it('return true if Nan', function () {
            expect(remick.LangUtil.getStringSafely(Number.NaN)).toBe(null);
            expect(remick.LangUtil.getStringSafely(1/'a')).toBe(null);
        });

        it('return the string for any type of boolean', function () {
            expect(remick.LangUtil.getStringSafely(true)).toBe('true');
            expect(remick.LangUtil.getStringSafely(false)).toBe('false');
        });

        it('return empty string for an empty string', function () {
            expect(remick.LangUtil.getStringSafely('')).toBe('');
        });

        it('return false for any string', function () {
            expect(remick.LangUtil.getStringSafely('a')).toBe('a');
        });

        it('return "0" for 0', function () {
            expect(remick.LangUtil.getStringSafely(0)).toBe('0');
        });

        // apparently the negitive sign gets lost in coersion.
        it('return "-0" for -0', function () {
            expect(remick.LangUtil.getStringSafely(-0)).toBe('0');
        });

        it('return string for any number', function () {
            expect(remick.LangUtil.getStringSafely(876)).toBe('876');
        });

        it('return null for any number', function () {
            expect(remick.LangUtil.getStringSafely(876)).toBe('876');
        });

    });
});

