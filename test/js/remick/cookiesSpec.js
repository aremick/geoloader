/* jshint jasmine: true */

describe('remick.cookies:', function() {
    'use strict';

    it('cookies should be defined in remick.cookies namespace.', function () {
    	expect(remick.cookies).toBeDefined();
    	expect(typeof remick.cookies.getCookie).toBe('function');
    });

	describe('remick.cookies.getCookie(): ', function () {

	    it('can handle the absense of cookies.', function () {
	    	expect(remick.cookies.getCookie('foo', '')).toBe(undefined);
	    	expect(remick.cookies.getCookie(null, '')).toEqual({});
	    });

	    it('with a single empty key.', function () {
	    	expect(remick.cookies.getCookie('', 'bar')).toBe('bar');
	    	expect(remick.cookies.getCookie(null, 'bar')).toEqual({'': 'bar'});
	    });

	    it('a single empty value.', function () {
	    	expect(remick.cookies.getCookie('foo', 'foo=')).toBe('');
	    	expect(remick.cookies.getCookie(null, 'foo=')).toEqual({'foo': ''});
	    });

	    it('test with keys and values.', function () {
	    	var cString = 'foo=bar; baz=boy';
	    	expect(remick.cookies.getCookie('foo', cString)).toBe('bar');
	    	expect(remick.cookies.getCookie('baz', cString)).toBe('boy');
	    	expect(remick.cookies.getCookie(null, cString)).toEqual({'foo': 'bar', 'baz': 'boy'});
	    });
	});

	describe('remick.cookies.makeCookie: ', function () {

		var makeCookie = remick.cookies.makeCookie;

	    it('setName() empty with a non-valid param', function () {
	        expect(makeCookie({'name': null})).toBe('=');
	        expect(makeCookie({'name': []})).toBe('=');
	        expect(makeCookie({'name': undefined})).toBe('=');
	    });

	    it('setName() with valid param', function () {
	        expect(makeCookie({'name': ''})).toBe('=');
	        expect(makeCookie({'name': 'bubba'})).toBe('bubba=');
	        expect(makeCookie({'name': '" sd;, ='})).toBe('%22%20sd%3B%2C%20%3D=');
	        expect(makeCookie({'name': 9})).toBe('9=');
	    });

	    it('setValue() empty with a non-valid param', function () {
	        expect(makeCookie({'value': null})).toBe('=');
	        expect(makeCookie({'value': []})).toBe('=');
	        expect(makeCookie({'value': undefined})).toBe('=');
	    });

	    it('setValue() with a valid param', function () {
	        expect(makeCookie({'value': ''})).toBe('=');
	        expect(makeCookie({'value': 'bubba'})).toBe('=bubba');
	        expect(makeCookie({'value': '" sd;, ='})).toBe('=%22%20sd%3B%2C%20%3D');
	        expect(makeCookie({'value': 9})).toBe('=9');
	    });

	    it('setPath() with a non-string param', function () {
	        expect(makeCookie({'path': null})).toBe('=');
	        expect(makeCookie({'path': undefined})).toBe('=');
	        expect(makeCookie({'path': ''})).toBe('=');
	        expect(makeCookie({'path': 0})).toBe('=');
	    });

	    it('setPath() with a valid param', function () {
	        expect(makeCookie({'path': '/'})).toBe('=;path=/');
	        expect(makeCookie({'path': 'bubba'})).toBe('=;path=bubba');
	        expect(makeCookie({'path': '/do/as/thou/wilt'})).toBe('=;path=/do/as/thou/wilt');
	        expect(makeCookie({'path': 9})).toBe('=;path=9');
	    });

	    it('setDomain() with a non-string param', function () {
	        expect(makeCookie({'domain': null})).toBe('=');
	        expect(makeCookie({'domain': undefined})).toBe('=');
	        expect(makeCookie({'domain': ''})).toBe('=');
	        expect(makeCookie({'domain': 0})).toBe('=');
	    });

	    it('setDomain() with a valid param', function () {
	        expect(makeCookie({'domain': '/'})).toBe('=;domain=/');
	        expect(makeCookie({'domain': 'bubba'})).toBe('=;domain=bubba');
	        expect(makeCookie({'domain': '/do/as/thou/wilt'})).toBe('=;domain=/do/as/thou/wilt');
	        expect(makeCookie({'domain': 9})).toBe('=;domain=9');
	    });

	    it('should set max-age, when maxAge is set.', function () {
	    	jasmine.clock().mockDate(new Date('Fri, 11 Dec 2015 06:18:55 GMT'));
	        expect(makeCookie({'maxAge': 60})).toBe('=;expires=Fri, 11 Dec 2015 06:19:55 GMT;max-age=60');
	        expect(makeCookie({'maxAge': -60})).toBe('=;expires=Fri, 11 Dec 2015 06:17:55 GMT;max-age=-60');
	        expect(makeCookie({'maxAge': 0})).toBe('=;expires=Fri, 11 Dec 2015 06:18:55 GMT;max-age=0');
	    });

	    it('add the secure flag when set to truthy', function () {
	        expect(makeCookie({'secure': undefined})).toBe('=');
	        expect(makeCookie({'secure':false})).toBe('=');
	        expect(makeCookie({'secure':1})).toBe('=;secure');
	        expect(makeCookie({'secure':true})).toBe('=;secure');
	    });
    });

	describe('remick.cookies.deleteCookie(): ', function () {
		var now;
    	// mock the clock
    	jasmine.clock().mockDate(new Date('Fri, 11 Dec 2015 06:18:55 GMT'));
        now = new Date().getTime();

		it('should have the right name', function() {
			expect(remick.cookies.deleteCookie('bubba', false)).toBe('bubba=;expires=Fri, 11 Dec 2015 06:17:16 GMT;max-age=-99');
	    });
	});

});
