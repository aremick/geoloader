/* jshint jasmine: true */

describe('adobe.Env:', function() {
    'use strict';

    it('env should be defined in the adobe namespace.', function () {
    	expect(adobe.Env).toBeDefined();
    	expect(typeof adobe.Env).toBe('object');
    });

	describe('adobe.Env.getEnv(): ', function () {
		it('check the non prod matching', function () {
			expect(adobe.Env.getEnv('127.0.0.1')).toBe('LOCAL');
			expect(adobe.Env.getEnv('127.0.0.1')).toBe('LOCAL');
			expect(adobe.Env.getEnv('localhost')).toBe('LOCAL');
			expect(adobe.Env.getEnv('www.dev.adobe.com')).toBe('DEV');
			expect(adobe.Env.getEnv('qa.something.else')).toBe('QA');
			expect(adobe.Env.getEnv('cqstage.adobe.com')).toBe('STAGE');
		});

		it('check the non prod matching', function () {
			expect(adobe.Env.getEnv('www.adobe.com')).toBe('PROD');
		});
	});

	describe('adobe.Env.isProd(): ', function () {
		it('check the non prod matching', function () {
			expect(adobe.Env.isProd('127.0.0.1')).toBe(false);
			expect(adobe.Env.isProd('127.0.0.1')).toBe(false);
			expect(adobe.Env.isProd('localhost')).toBe(false);
			expect(adobe.Env.isProd('www.DEV.adobe.com')).toBe(false);
			expect(adobe.Env.isProd('qa.something.else')).toBe(false);
			expect(adobe.Env.isProd('cqstage.adobe.com')).toBe(false);
		});

		it('check the non prod matching', function () {
			expect(adobe.Env.isProd('www.adobe.com')).toBe(true);
		});
	});
});
